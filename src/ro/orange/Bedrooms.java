package ro.orange;

public class Bedrooms {

    public static void main(String[] args) {

        HotelBedrooms bed1 = new HotelBedrooms();
        HotelBedrooms bed2 = new HotelBedrooms();
        System.out.println("Start");
        bed1.setDimension(100);     // initializare Dimension
        bed1.setHeight(200);        // initializare Height
        bed1.setWeight(152);        // initializare Weight
        bed1.setStyle("Headboard and Side Rails");  // initializare Style
        bed2.setDimension(100);
        bed2.setHeight(100);
        bed2.setWeight(190);
        bed2.setStyle("Swedish");
        bed1.CalculateSize();
        bed2.CalculateSize();
        bed1.getStyle();
        bed2.getStyle();


    }
}
