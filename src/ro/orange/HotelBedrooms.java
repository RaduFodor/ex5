package ro.orange;

import java.util.PrimitiveIterator;

public class HotelBedrooms implements MyInterface {
    // variables declaration
    private int dimension;
    private int height;
    private int weight;
    private int Size;
    private String Style;


    public int getDimension() {
        return dimension;
    }

    public int getHeight() {
        return height;
    }

    public int getWeight() {
        return weight;
    }

    public int getSize() {
        return Size;
    }

    public void setDimension(int dimension) {
        this.dimension = dimension;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public void setSize(int size) {
        Size = size;
    }

    public void setStyle(String style) {
        Style = style;
    }

    // Size = dimension + height + weight ???
    public int CalculateSize(){
        this.Size = this.dimension + this.height + this.weight;
        System.out.println(this.Size);
        return this.Size;
            }


    @Override
    public void getStyle() {
        System.out.println("The style of this bed is called " + this.Style);
           }
}
